﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqWebApi.Interfaces
{
    interface ITasksService
    {
        List<Models.Task> GetTasks();
        Models.Task GetTask(int id);
    }
}
