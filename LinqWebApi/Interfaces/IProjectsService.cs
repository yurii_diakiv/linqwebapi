﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Interfaces
{
    interface IProjectsService
    {
        List<Project> GetProjects();
        Project GetProject(int id);
    }
}
