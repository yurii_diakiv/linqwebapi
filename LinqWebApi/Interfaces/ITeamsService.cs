﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Interfaces
{
    interface ITeamsService
    {
        List<Team> GetTeams();
        Team GetTeam(int id);
    }
}
