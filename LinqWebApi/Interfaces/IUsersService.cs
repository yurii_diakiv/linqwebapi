﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Interfaces
{
    interface IUsersService
    {
        List<User> GetUsers();
        User GetUser(int id);
    }
}
