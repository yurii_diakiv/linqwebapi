﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
using LinqWebApi.Models;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private TaskStatesService taskStatesService;

        public TaskStatesController(TaskStatesService service)
        {
            taskStatesService = service;
        }

        [HttpGet]
        public List<TaskState> Get()
        {
            return taskStatesService.GetTaskStates();
        }

        [HttpGet("{id}")]
        public TaskState Get(int id)
        {
            return taskStatesService.GetTaskState(id);
        }

        [HttpPost]
        public void Post([FromBody] TaskState taskState)
        {
            taskStatesService.Create(taskState);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] TaskState taskState)
        {
            taskStatesService.Update(id, taskState);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            taskStatesService.Delete(id);
        }
    }
}
