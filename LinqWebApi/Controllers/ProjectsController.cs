﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
using LinqWebApi.Models;
using LinqWebApi.Repositories;
using Newtonsoft.Json;
using System.Net.Http;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private ProjectsService projectsService;

        public ProjectsController(ProjectsService service)
        {
            projectsService = service;
        }

        [HttpGet]
        public List<Project> Get()
        {
            return projectsService.GetProjects();
        }

        [HttpGet("{id}")]
        public Project Get(int id)
        {
            return projectsService.GetProject(id);
        }

        [HttpPost]
        public void Post([FromBody] Project project)
        {
            projectsService.Create(project);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Project project)
        {
            projectsService.Update(id, project);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            projectsService.Delete(id);
        }
    }
}
