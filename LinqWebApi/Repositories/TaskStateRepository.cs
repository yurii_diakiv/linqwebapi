﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Models;

namespace LinqWebApi.Repositories
{
    public class TaskStateRepository : IRepository<TaskState>
    {
        public LinqWebApi.Data data = new LinqWebApi.Data();
        List<TaskState> taskStates;

        public TaskStateRepository()
        {
            taskStates = data.GetTaskStates().ToList();
        }
        public List<TaskState> GetItemList()
        {
            return taskStates;
        }

        public TaskState GetItem(int id)
        {
            return taskStates.Find(ts => ts.Id == id);
        }

        public void Create(TaskState ts)
        {
            taskStates.Add(ts);
        }

        public void Update(int id, TaskState t)
        {
            int i = taskStates.FindIndex(ts => ts.Id == id);
            taskStates[i] = t;
        }

        public void Delete(int id)
        {
            TaskState t = taskStates.Find(ts => ts.Id == id);
            if (t != null)
            {
                taskStates.Remove(t);
            }
        }
    }
}
