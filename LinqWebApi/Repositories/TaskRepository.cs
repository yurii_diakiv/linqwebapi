﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqWebApi.Repositories
{
    public class TaskRepository : IRepository<Models.Task>
    {
        public LinqWebApi.Data data = new LinqWebApi.Data();
        List<Models.Task> tasks;

        public TaskRepository()
        {
            tasks = data.GetTasks().ToList();
        }
        public List<Models.Task> GetItemList()
        {
            return tasks;
        }

        public Models.Task GetItem(int id)
        {
            return tasks.Find(p => p.Id == id);
        }

        public void Create(Models.Task t)
        {
            tasks.Add(t);
        }

        public void Update(int id, Models.Task t)
        {
            int i = tasks.FindIndex(ta => ta.Id == id);
            tasks[i] = t;
        }

        public void Delete(int id)
        {
            Models.Task t = tasks.Find(ta => ta.Id == id);
            if (t != null)
            {
                tasks.Remove(t);
            }
        }
    }
}
